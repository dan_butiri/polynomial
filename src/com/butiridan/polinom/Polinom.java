package com.butiridan.polinom;

import java.util.HashMap;

/**
 * @author Dan
 * 
 */
public class Polinom {
	private HashMap<Integer, Double> polinom;
	private Integer gradMax;
	private String name;

	/**
	 * Constructor pt. Clasa Polinom.
	 */
	Polinom() {
		polinom = new HashMap<Integer, Double>();
		name = new String();
		gradMax = new Integer(0);
	}

	/**
	 * Constructor pt. Clasa Polinom.
	 * 
	 * @param name
	 *            Numele polinomului.
	 */
	Polinom(String name) {
		polinom = new HashMap<Integer, Double>();
		this.name = name;
		gradMax = new Integer(0);
	}

	/**
	 * 
	 * Adaugarea de element (monom) in HashMap.
	 * 
	 * @param coeficent
	 *            Coeficentul monomului care se adauga.
	 * @param exponent
	 *            Exponentul monomului care se adauga.
	 */
	public void add(double coeficent, int exponent) {
		if(gradMax.intValue() != 0 || coeficent != 0.0){
		polinom.put(new Integer(exponent), new Double(coeficent));
		if (exponent > gradMax.intValue())
			gradMax = new Integer(exponent);
	}else{
		polinom.put(new Integer(0), new Double(0));
	}
	}

	/**
	 * Verifica daca exista un anuit element in HashMap.
	 * 
	 * @param Exponent
	 *            exponentului monomului care doreste sa fie verificat.
	 * @return true - daca a fost gasit. false - daca nu a fost gasit.
	 */
	public boolean existaExponent(int exponent) {
		return polinom.containsKey(new Integer(exponent));
	}

	/**
	 * Coeficentului monomului de exponent specificat (prin exponent).
	 * 
	 * @param exponent
	 *            Exponentul monomului pt.care doreste sa se afle coeficentul.
	 * @return Coeficentul monomului de grad exponent.
	 */
	public double getCoeficent(int exponent) {
		return polinom.get(new Integer(exponent));
	}

	/**
	 * Numarul de monoame ale unui polinom.
	 * 
	 * @return Nr. de monoame existente in HashMap (polinom).
	 */
	public int numarMonoame() {
		return polinom.size();
	}

	/**
	 * Exponentului maxim din polinom.
	 * 
	 * @return Exponentul maxim din polinomului.
	 */
	public int getGradMax() {
		return gradMax.intValue();
	}

	/**
	 * Modifica numele polinomului.
	 * 
	 * @param name
	 *            Numele polinomului.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Da numele polinomului.
	 * 
	 * @return Nume polinom.
	 */
	public String getName() {
		return name;
	}

}
