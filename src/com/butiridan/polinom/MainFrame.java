package com.butiridan.polinom;

import javax.swing.JFrame;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class MainFrame extends JFrame {

	private MainPanel contentPane;

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new MainPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setTitle("Polinom!");
		this.setResizable(false);
		this.setContentPane(contentPane);
		this.pack();
	}

	/**
	 * Preluarea Panel-ului principal (MainPanel).
	 * 
	 * @return Panel-ul.
	 */
	public MainPanel getMainPanel() {
		return contentPane;
	}

}
