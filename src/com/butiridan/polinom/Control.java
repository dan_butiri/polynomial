package com.butiridan.polinom;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Control {

	private MainFrame mainFrame;
	private MyPanel getPoli1;
	private MyPanel getPoli2;
	private JTextField tfRezultat;
	private JTextField tfRezultatPoli;
	private JTextField tfValoareX;
	private JTextField tfPutereP;
	private Polinom polinom1 = new Polinom("P1");
	private Polinom polinom2 = new Polinom("P2");
	private Polinom polinom3 = new Polinom("F");
	private Operatori operator = new Operatori();

	/**
	 * Construcort Control.
	 * 
	 * @param mf
	 *            Panal-ul principal.
	 */
	public Control(MainFrame mf) {
		mainFrame = mf;
		getPoli1 = mf.getMainPanel().getPolinom1();
		getPoli2 = mf.getMainPanel().getPolinom2();
		tfRezultat = mf.getMainPanel().gettfRezultat();
		tfRezultatPoli = mf.getMainPanel().gettfRezultatPoli();
		tfValoareX = mf.getMainPanel().gettfValoareX();
		tfPutereP = mf.getMainPanel().gettfPutereP();

		getPoli1.addbtnVerificaActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg) {
				schimba(getPoli1);
				polinom1 = new Polinom(polinom1.getName());
				proceseaza(getPoli1, polinom1);
			}
		});

		getPoli1.addbtnVerificaKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent arg) {
				if (arg.getKeyCode() == KeyEvent.VK_ENTER) {
					schimba(getPoli1);
					polinom1 = new Polinom(polinom1.getName());
					proceseaza(getPoli1, polinom1);
				}
			}
		});

		getPoli1.addtfCoefPolinomKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent arg) {
				if (arg.getKeyCode() == KeyEvent.VK_ENTER) {
					schimba(getPoli1);
					polinom1 = new Polinom(polinom1.getName());
					proceseaza(getPoli1, polinom1);
				}
			}
		});

		getPoli2.addbtnVerificaActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg) {
				schimba(getPoli2);
				polinom2 = new Polinom(polinom2.getName());
				proceseaza(getPoli2, polinom2);
			}
		});

		getPoli2.addbtnVerificaKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent arg) {
				if (arg.getKeyCode() == KeyEvent.VK_ENTER) {
					schimba(getPoli2);
					polinom2 = new Polinom(polinom2.getName());
					proceseaza(getPoli2, polinom2);
				}
			}
		});

		getPoli2.addtfCoefPolinomKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent arg) {
				if (arg.getKeyCode() == KeyEvent.VK_ENTER) {
					schimba(getPoli2);
					polinom2 = new Polinom(polinom2.getName());
					proceseaza(getPoli2, polinom2);
				}
			}
		});

		mf.getMainPanel().addbtnAdunareActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg) {
				polinom3 = operator.adunare(polinom1, polinom2);
				tfRezultat.setText(formatPolinom(polinom3));
				tfRezultatPoli.setText(polinom3.getName() + "(x) = "
						+ formatPoli(polinom3));
			}
		});

		mf.getMainPanel().addbtnScadereActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg) {
				polinom3 = operator.scadere(polinom1, polinom2);
				tfRezultat.setText(formatPolinom(polinom3));
				tfRezultatPoli.setText(polinom3.getName() + "(x) = "
						+ formatPoli(polinom3));
			}
		});

		mf.getMainPanel().addbtnImultireActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg) {
				polinom3 = operator.imultire(polinom1, polinom2);
				tfRezultat.setText(formatPolinom(polinom3));
				tfRezultatPoli.setText(polinom3.getName() + "(x) = "
						+ formatPoli(polinom3));
			}
		});

		mf.getMainPanel().addbtnDerivareActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg) {
				polinom3 = operator.derivare(polinom1);
				tfRezultat.setText(formatPolinom(polinom3));
				tfRezultatPoli.setText(polinom3.getName() + "(x) = "
						+ formatPoli(polinom3));
			}
		});

		mf.getMainPanel().addbtnIntegrareActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg) {
				polinom3 = operator.integrare(polinom1);
				tfRezultat.setText(formatPolinom(polinom3));
				tfRezultatPoli.setText(polinom3.getName() + "(x) = "
						+ formatPoli(polinom3));
			}
		});

		mf.getMainPanel().addbtnImpartireActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg) {
				polinom3 = operator.impartire(polinom1, polinom2);
				tfRezultat.setText(formatPolinom(polinom3));
				tfRezultatPoli.setText(polinom3.getName() + "(x) = "
						+ formatPoli(polinom3));
			}
		});

		mf.getMainPanel().addmodImpartireActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg) {
				polinom3 = operator.modImpartire(polinom1, polinom2);
				tfRezultat.setText(formatPolinom(polinom3));
				tfRezultatPoli.setText(polinom3.getName() + "(x) = "
						+ formatPoli(polinom3));
			}
		});

		mf.getMainPanel().addbtnCalculeazaActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg) {
				double x;
				try {
					x = operator.valoarePolinomX(polinom1,
							Double.parseDouble(tfValoareX.getText()));
					tfRezultat.setText(polinom1.getName() + "("
							+ tfValoareX.getText() + ")" + "="
							+ new DecimalFormat("#.##").format(x));
					tfRezultatPoli.setText("");
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null,
							"Date de intrare incorecte!\n" + e.toString());
				}
			}
		});

		mf.getMainPanel().addtfValoareXKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent arg) {
				if (arg.getKeyCode() == KeyEvent.VK_ENTER) {
					double x;
					try {
						x = operator.valoarePolinomX(polinom1,
								Double.parseDouble(tfValoareX.getText()));
						tfRezultat.setText(polinom1.getName() + "("
								+ tfValoareX.getText() + ")" + "="
								+ new DecimalFormat("#.##").format(x));
						tfRezultatPoli.setText("");
					} catch (Exception e) {
						JOptionPane.showMessageDialog(null,
								"Date de intrare incorecte!\n" + e.toString());
					}
				}
			}
		});

		mf.getMainPanel().addbtnPutereActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg) {
				try {
					polinom3 = operator.putere(polinom1,
							Integer.parseInt(tfPutereP.getText()));
					tfRezultat.setText(formatPolinom(polinom3));
					tfRezultatPoli.setText(polinom3.getName() + "(x) = "
							+ formatPoli(polinom3));
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null,
							"Date de intrare incorecte!\n" + e.toString());
				}
			}
		});

		mf.getMainPanel().addtfPuterePKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent arg) {
				try {
					if (arg.getKeyCode() == KeyEvent.VK_ENTER) {
						polinom3 = operator.putere(polinom1,
								Integer.parseInt(tfPutereP.getText()));
						tfRezultat.setText(formatPolinom(polinom3));
						tfRezultatPoli.setText(polinom3.getName() + "(x) = "
								+ formatPoli(polinom3));
					}
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null,
							"Date de intrare incorecte!\n" + e.toString());
				}
			}
		});

		mf.getMainPanel().addbtnExtindeActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg) {
				if (mainFrame.getMainPanel().getbtnExtinde().getText()
						.compareTo("Extinde") == 0) {
					mainFrame.getMainPanel().getScroll().setVisible(true);
					mainFrame.pack();
					mainFrame.getMainPanel().getbtnExtinde()
							.setText("Restrange");
				} else {
					mainFrame.getMainPanel().getScroll().setVisible(false);
					mainFrame.pack();
					mainFrame.getMainPanel().getbtnExtinde().setText("Extinde");
				}
			}
		});
	}

	private void schimba(MyPanel myPanel) {
		if (myPanel.getbtnVerificaText().compareTo("Verifica!") == 0) {
			myPanel.setbtnVerificaText("Schimba!");
			myPanel.settfNumePolinomVisible(false);
			myPanel.settfCoefPolinomVisible(false);
			myPanel.setlbPolinomVisible(true);
		} else if (myPanel.getbtnVerificaText().compareTo("Schimba!") == 0) {
			myPanel.setbtnVerificaText("Verifica!");
			myPanel.settfNumePolinomVisible(true);
			myPanel.settfCoefPolinomVisible(true);
			myPanel.setlbPolinomVisible(false);
		}
	}

	private void proceseaza(MyPanel myPanel, Polinom polinom) {
		if (myPanel.getbtnVerificaText().compareTo("Verifica!") != 0) {
			if (myPanel.gettfNumePolinom().compareTo("Nume polinom") == 0) {
				myPanel.setlbPolinomText(polinom.getName() + "(x) = "
						+ formatare(myPanel, polinom));
			} else {
				myPanel.setlbPolinomText(myPanel.gettfNumePolinom() + "(x) = "
						+ formatare(myPanel, polinom));
			}
		}
	}

	private String formatare(MyPanel myPanel, Polinom polinom) {
		String[] coef = myPanel.gettfCoefPolinom().toString().split("[ ]+");

		try {
			if (myPanel.gettfCoefPolinom().compareTo("Coeficenti polinom") != 0) {
				for (int i = 0; i < coef.length; ++i) {
					polinom.add(Double.parseDouble(coef[i]),
							(coef.length - i - 1));
				}
			}

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Date de intrare incorecte!\n"
					+ e.toString());
		}

		return formatPoli(polinom);
	}

	private String formatPoli(Polinom polinom) {
		String polinomString = "";
		double coeficent;

		for (int i = polinom.getGradMax(); i >= 0; --i) {
			if (polinom.existaExponent(i)) {
				coeficent = polinom.getCoeficent(i);

				if (coeficent != 0) {
					if (coeficent > 0.0) {

						if (coeficent != 1.0) {

							if (i != 1) {
								if (i != 0) {
									polinomString += (" + "
											+ new DecimalFormat("#.##")
													.format(coeficent) + "x^" + i);
								} else {
									polinomString += (" + " + new DecimalFormat(
											"#.##").format(coeficent));
								}
							} else {
								polinomString += (" + "
										+ new DecimalFormat("#.##")
												.format(coeficent) + "x");
							}
						} else {

							if (i != 1) {
								if (i != 0) {
									polinomString += (" + " + "x^" + i);
								} else {
									polinomString += (" + " + new DecimalFormat(
											"#.##").format(coeficent));
								}
							} else {
								polinomString += (" + " + "x");
							}
						}
					} else {
						// if((coef.length - 1) == i) polinomString += ("-");
						if (coeficent != -1.0) {

							if (i != 1) {
								if (i != 0) {
									polinomString += (" "
											+ new DecimalFormat("#.##")
													.format(coeficent) + "x^" + i);
								} else {
									polinomString += (" " + new DecimalFormat(
											"#.##").format(coeficent));
								}
							} else {
								polinomString += (" "
										+ new DecimalFormat("#.##")
												.format(coeficent) + "x");
							}
						} else {

							if (i != 1) {
								if (i != 0) {
									polinomString += (" -" + "x^" + i);
								} else {
									polinomString += (" " + new DecimalFormat(
											"#.##").format(coeficent));
								}
							} else {
								polinomString += (" -" + "x");
							}
						}
					}
				}
			}
		}

		if (polinomString.length() < 2)
			polinomString += " + 0";
		polinomString = polinomString.substring(2);

		return polinomString;
	}

	private String formatPolinom(Polinom polinom) {
		String aux = "";

		for (int i = polinom.getGradMax(); i >= 0; --i) {
			if (polinom.existaExponent(i)) {
				aux += (new DecimalFormat("#.##").format(polinom
						.getCoeficent(i)) + " ");
			} else {
				aux += "0 ";
			}
		}
		return aux;
	}
}
