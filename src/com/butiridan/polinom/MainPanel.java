package com.butiridan.polinom;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class MainPanel extends JPanel {

	private GridBagConstraints gbc = new GridBagConstraints();
	private MyPanel polinom1 = new MyPanel();
	private MyPanel polinom2 = new MyPanel();
	private JTextField tfRezultat = new JTextField();
	private JTextField tfRezultatPoli = new JTextField();
	private JButton btnAdunare = new JButton("Adunare");
	private JButton btnScadere = new JButton("Scadere");
	private JButton btnImultire = new JButton("Imultire");
	private JButton btnImpartire = new JButton("Impartire");
	private JButton btnDerivare = new JButton("Derivare");
	private JButton btnIntegrare = new JButton("Integrare");
	private JButton btnCalculeaza = new JButton("Calculeaza");
	private JButton btnModImpartire = new JButton("ModImpartire");
	private JButton btnExtinde = new JButton("Extinde");
	private JTextField tfValoareX = new JTextField("Val. X");
	private JButton btnPutere = new JButton("Putere");
	private JTextField tfPutereP = new JTextField("Val. p");
	private JTextArea taText = new JTextArea();
	private JScrollPane scroll = new JScrollPane(taText);

	/**
	 * Create the panel.
	 */
	public MainPanel() {
		
		setLayout(new GridBagLayout());

		gbc.insets = new Insets(5, 5, 5, 5);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridwidth = 6;
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		this.add(tfRezultatPoli, gbc);
		tfRezultatPoli.setEditable(false);
		tfRezultatPoli.setHorizontalAlignment(SwingConstants.CENTER);
		tfRezultatPoli.setPreferredSize(new Dimension(700, 30));
		tfRezultatPoli.setMaximumSize(new Dimension(700, 30));
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		this.add(tfRezultat, gbc);
		tfRezultat.setEditable(false);
		tfRezultat.setHorizontalAlignment(SwingConstants.CENTER);
		tfRezultat.setPreferredSize(new Dimension(700, 30));
		tfRezultat.setMaximumSize(new Dimension(700, 30));
		
		gbc.insets = new Insets(10, 17, 10, 17);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridwidth = 1;
		
		gbc.gridx = 0;
		gbc.gridy = 2;
		this.add(btnCalculeaza, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 2;
		this.add(tfValoareX, gbc);
		tfValoareX.setHorizontalAlignment(SwingConstants.CENTER);
		tfValoareX.setPreferredSize(new Dimension(100, 25));
		tfValoareX.setMaximumSize(new Dimension(100, 25));
		tfValoareX.setForeground(Color.LIGHT_GRAY);
		tfValoareX.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent arg) {
				if(tfValoareX.getText().compareTo("Val. X") == 0){
					tfValoareX.setForeground(Color.black);
					tfValoareX.setText("");
				}
			}
			public void focusLost(FocusEvent arg) {
				if(tfValoareX.getText().compareTo("") == 0){
					tfValoareX.setForeground(Color.LIGHT_GRAY);
					tfValoareX.setText("Val. X");
				}
			}
		});
		
		gbc.gridx = 0;
		gbc.gridy = 3;
		this.add(btnPutere, gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 3;
		this.add(tfPutereP, gbc);
		tfPutereP.setHorizontalAlignment(SwingConstants.CENTER);
		tfPutereP.setPreferredSize(new Dimension(100, 25));
		tfPutereP.setMaximumSize(new Dimension(100, 25));
		tfPutereP.setForeground(Color.LIGHT_GRAY);
		tfPutereP.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent arg) {
				if(tfPutereP.getText().compareTo("Val. p") == 0){
					tfPutereP.setForeground(Color.black);
					tfPutereP.setText("");
				}
			}
			public void focusLost(FocusEvent arg) {
				if(tfPutereP.getText().compareTo("") == 0){
					tfPutereP.setForeground(Color.LIGHT_GRAY);
					tfPutereP.setText("Val. p");
				}
			}
		});

		gbc.gridx = 2;
		gbc.gridy = 2;
		this.add(btnAdunare, gbc);

		gbc.gridx = 2;
		gbc.gridy = 3;
		this.add(btnScadere, gbc);

		gbc.gridx = 3;
		gbc.gridy = 2;
		this.add(btnImultire, gbc);

		gbc.gridx = 3;
		gbc.gridy = 3;
		this.add(btnImpartire, gbc);

		gbc.gridx = 4;
		gbc.gridy = 2;
		this.add(btnDerivare, gbc);

		gbc.gridx = 4;
		gbc.gridy = 3;
		this.add(btnIntegrare, gbc);
		
		gbc.gridx = 5;
		gbc.gridy = 2;
		this.add(btnModImpartire, gbc);
		
		gbc.gridx = 5;
		gbc.gridy = 3;
		this.add(btnExtinde, gbc);
		
		gbc.insets = new Insets(5, 5, 5, 5);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridwidth = 6;
		
		gbc.gridx = 0;
		gbc.gridy = 4;
		this.add(polinom1, gbc);

		gbc.gridx = 0;
		gbc.gridy = 5;
		this.add(polinom2, gbc);
		
		gbc.ipady = 200;
		
		gbc.gridx = 0;
		gbc.gridy = 6;
		this.add(scroll, gbc);
		scroll.setVisible(false);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	    
	}

	/**
	 * Panel-ul pt. introducerea Polinomului 1.
	 * 
	 * @return Panel-ul.
	 */
	public MyPanel getPolinom1() {
		return polinom1;
	}

	/**
	 * Panel-ul pt. introducerea Polinomului 2.
	 * 
	 * @return Panel-ul.
	 */
	public MyPanel getPolinom2() {
		return polinom2;
	}

	/**
	 * Panel-ul pt. introducerea Polinomului 3.
	 * 
	 * @return Panel-ul.
	 */
	public JTextField gettfRezultat() {
		return tfRezultat;
	}
	
	public JTextField gettfRezultatPoli() {
		return tfRezultatPoli;
	}
	
	public JTextField gettfValoareX() {
		return tfValoareX;
	} 
	
	public JTextField gettfPutereP() {
		return tfPutereP;
	}
	public JScrollPane getScroll() {
		return scroll;
	}
	
	public JButton getbtnExtinde() {
		return btnExtinde;
	}
	

	/**
	 * Adauga ActionListener pt. butonul Adunare.
	 * 
	 * @param al
	 */
	public void addbtnAdunareActionListener(ActionListener al) {
		btnAdunare.addActionListener(al);
	}

	/**
	 * Adauga ActionListener pt. butonul Scadere.
	 * 
	 * @param al
	 */
	public void addbtnScadereActionListener(ActionListener al) {
		btnScadere.addActionListener(al);
	}

	/**
	 * Adauga ActionListener pt. butonul Imultire.
	 * 
	 * @param al
	 */
	public void addbtnImultireActionListener(ActionListener al) {
		btnImultire.addActionListener(al);
	}

	/**
	 * Adauga ActionListener pt. butonul Impartire.
	 * 
	 * @param al
	 */
	public void addbtnImpartireActionListener(ActionListener al) {
		btnImpartire.addActionListener(al);
	}

	/**
	 * Adauga ActionListener pt. butonul Derivare.
	 * 
	 * @param al
	 */
	public void addbtnDerivareActionListener(ActionListener al) {
		btnDerivare.addActionListener(al);
	}

	/**
	 * Adauga ActionListener pt. butonul Integrare.
	 * 
	 * @param al
	 */
	public void addbtnIntegrareActionListener(ActionListener al) {
		btnIntegrare.addActionListener(al);
	}
	
	public void addbtnCalculeazaActionListener(ActionListener al) {
		btnCalculeaza.addActionListener(al);
	}
	
	public void addbtnPutereActionListener(ActionListener al) {
		btnPutere.addActionListener(al);
	}
	
	public void addmodImpartireActionListener(ActionListener al) {
		btnModImpartire.addActionListener(al);
	}
	
	public void addtfValoareXKeyListener(KeyAdapter ka){
		tfValoareX.addKeyListener(ka);
	}
	
	public void addtfPuterePKeyListener(KeyAdapter ka){
		tfPutereP.addKeyListener(ka);
	}

	public void addbtnExtindeActionListener(ActionListener al) {
		btnExtinde.addActionListener(al);
	}
}
