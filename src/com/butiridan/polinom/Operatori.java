package com.butiridan.polinom;

import java.lang.Math;

import javax.swing.JOptionPane;

public class Operatori {

	/**
	 * Adunarea a 2 polinoame.
	 * 
	 * @param x
	 *            Primul polinom
	 * @param y
	 *            Al 2-lea polinom.
	 * @return Polinomul rezultat.
	 */
	public Polinom adunare(Polinom x, Polinom y) {
		Polinom z = new Polinom("f");

		for (int i = 0; i <= getMaxim(x.getGradMax(), y.getGradMax()); ++i) {
			if (x.existaExponent(i) && y.existaExponent(i)) {
				z.add((x.getCoeficent(i) + y.getCoeficent(i)), i);
			} else if (x.existaExponent(i)) {
				z.add(x.getCoeficent(i), i);
			} else if (y.existaExponent(i)) {
				z.add(y.getCoeficent(i), i);
			}
		}
		return z;
	}

	/**
	 * Scaderea a 2 polinoame.
	 * 
	 * @param x
	 *            Primul polinom
	 * @param y
	 *            Al 2-lea polinom.
	 * @return Polinomul rezultat.
	 */
	public Polinom scadere(Polinom x, Polinom y) {
		Polinom z = new Polinom("f");

		for (int i = 0; i <= getMaxim(x.getGradMax(), y.getGradMax()); ++i) {
			if (x.existaExponent(i) && y.existaExponent(i)) {
				if (x.getCoeficent(i) - y.getCoeficent(i) != 0) {
					z.add((x.getCoeficent(i) - y.getCoeficent(i)), i);
				}
			} else if (x.existaExponent(i)) {
				z.add(x.getCoeficent(i), i);
			} else if (y.existaExponent(i)) {
				z.add((-y.getCoeficent(i)), i);
			}
		}
		return z;
	}

	/**
	 * Imultirea a 2 polinoame.
	 * 
	 * @param x
	 *            Primul polinom
	 * @param y
	 *            Al 2-lea polinom.
	 * @return Polinomul rezultat.
	 */
	public Polinom imultire(Polinom x, Polinom y) {
		Polinom z = new Polinom("f");
		double produsCoef;

		for (int i = 0; i <= x.getGradMax(); ++i) {
			for (int j = 0; j <= y.getGradMax(); ++j) {
				if (x.existaExponent(i) && y.existaExponent(j)) {
					produsCoef = x.getCoeficent(i) * y.getCoeficent(j);
					if (z.existaExponent(i + j)) {
						produsCoef = z.getCoeficent(i + j) + produsCoef;
					}
					z.add(produsCoef, i + j);
				}
			}
		}
		return z;
	}

	/**
	 * Valoarea intr-un anumit punct a polinomului.
	 * 
	 * @param x
	 *            Polinomul.
	 * @param valoare
	 *            Punctul in care sa se calculeze.
	 * @return Valoarea polinomului.
	 */
	public double valoarePolinomX(Polinom x, double valoare) {
		double valPoli = 0;

		for (int i = 0; i <= x.getGradMax(); ++i) {
			if (x.existaExponent(i)) {
				valPoli += (Math.pow(valoare, i) * x.getCoeficent(i));
			}
		}
		return valPoli;
	}

	/**
	 * Derivarea unui polinom.
	 * 
	 * @param x
	 *            Polinomul.
	 * @return Polinomul derivat.
	 */
	public Polinom derivare(Polinom x) {
		Polinom z = new Polinom("f");

		for (int i = 1; i <= x.getGradMax(); ++i) {
			if (x.existaExponent(i)) {
				z.add((x.getCoeficent(i) * i), (i - 1));
			}
		}
		return z;
	}

	public Polinom integrare(Polinom x) {
		Polinom z = new Polinom("f");

		for (int i = 0; i <= x.getGradMax(); ++i) {
			if (x.existaExponent(i)) {
				z.add((x.getCoeficent(i) / (i + 1)), (i + 1));
			}
		}
		return z;
	}

	public Polinom putere(Polinom x, int p) {
		Polinom z = new Polinom("f");

		if (p > 1) {
			z = imultire(x, x);
		}
		for (int i = 0; i < p - 2; ++i) {
			z = imultire(z, x);
		}
		return z;
	}

	public Polinom impartire(Polinom x, Polinom y) {
		Polinom zCat = new Polinom("f");
		Polinom zAux = new Polinom("f");
		double coef;
		int exp, gradX = x.getGradMax();

		while (gradX >= y.getGradMax()) {
			if (!y.existaExponent(y.getGradMax())) {
				JOptionPane.showMessageDialog(null,
						"Nu se poate imparti cu 0...");
				return zCat;
			}
			if (y.getCoeficent(y.getGradMax()) == 0.0) {
				JOptionPane.showMessageDialog(null,
						"Nu se poate imparti cu 0...");
				return zCat;
			}

			if (y.getGradMax() != 0) {
				exp = gradX - y.getGradMax();
				System.out.println(gradX);
				coef = x.getCoeficent(gradX) / y.getCoeficent(y.getGradMax());
				zCat.add(coef, exp);

				zAux = new Polinom("f");
				zAux.add(coef, exp);
				x = scadere(x, imultire(y, zAux));

				gradX = x.getGradMax();
			} else {
				for (int i = 0; i <= gradX; ++i) {
					if (x.existaExponent(i)) {
						zCat.add(
								x.getCoeficent(i)
										/ y.getCoeficent(y.getGradMax()), i);
					}
				}
				return zCat;
			}

		}

		return zCat;
	}

	public Polinom modImpartire(Polinom x, Polinom y) {
		Polinom zCat = new Polinom("f");
		Polinom zAux = new Polinom("f");
		double coef;
		int exp, gradX = x.getGradMax();

		while (gradX >= y.getGradMax()) {
			if (!y.existaExponent(y.getGradMax())) {
				JOptionPane.showMessageDialog(null,
						"Nu se poate imparti cu 0...");
				return x;
			}
			if (y.getCoeficent(y.getGradMax()) == 0.0) {
				JOptionPane.showMessageDialog(null,
						"Nu se poate imparti cu 0...");
				return x;
			}

			if (y.getGradMax() != 0) {
				exp = gradX - y.getGradMax();
				System.out.println(gradX);
				coef = x.getCoeficent(gradX) / y.getCoeficent(y.getGradMax());
				zCat.add(coef, exp);

				zAux = new Polinom("f");
				zAux.add(coef, exp);
				x = scadere(x, imultire(y, zAux));

				gradX = x.getGradMax();
			} else {
				for (int i = 0; i <= gradX; ++i) {
					if (x.existaExponent(i)) {
						zCat.add(
								x.getCoeficent(i)
										/ y.getCoeficent(y.getGradMax()), i);
					}
				}
				return x;
			}

		}

		return x;
	}

	private int getMaxim(int x, int y) {
		if (x >= y)
			return x;
		else
			return y;
	}
}
