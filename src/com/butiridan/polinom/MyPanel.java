package com.butiridan.polinom;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;

@SuppressWarnings("serial")
public class MyPanel extends JPanel {

	private GridBagConstraints gbc = new GridBagConstraints();
	private JTextField tfNumePolinom = new JTextField("Nume polinom");
	private JTextField tfCoefPolinom = new JTextField("Coeficenti polinom");
	private JButton btnVerifica = new JButton("Verifica!");
	private JLabel lbPolinom = new JLabel("0");
	
	/**
	 * Create the panel.
	 */
	public MyPanel() {
		this.setLayout(new GridBagLayout());
		this.setMaximumSize(new Dimension(700, 30));
		
		gbc.insets = new Insets(5, 5, 5, 5);
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		this.add(tfNumePolinom, gbc);
		tfNumePolinom.setHorizontalAlignment(SwingConstants.CENTER);
		tfNumePolinom.setPreferredSize(new Dimension(100, 25));
		tfNumePolinom.setMaximumSize(new Dimension(100, 25));
		tfNumePolinom.setForeground(Color.LIGHT_GRAY);
		tfNumePolinom.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent arg) {
				if(tfNumePolinom.getText().compareTo("Nume polinom") == 0){
					tfNumePolinom.setForeground(Color.black);
					tfNumePolinom.setText("");
				}
			}
			public void focusLost(FocusEvent arg) {
				if(tfNumePolinom.getText().compareTo("") == 0){
					tfNumePolinom.setForeground(Color.LIGHT_GRAY);
					tfNumePolinom.setText("Nume polinom");
				}
			}
		});
		
		gbc.gridx = 1;
		gbc.gridy = 0;
		this.add(tfCoefPolinom, gbc);
		tfCoefPolinom.setHorizontalAlignment(SwingConstants.CENTER);
		tfCoefPolinom.setPreferredSize(new Dimension(500, 25));
		tfCoefPolinom.setMaximumSize(new Dimension(500, 25));
		tfCoefPolinom.setForeground(Color.LIGHT_GRAY);
		tfCoefPolinom.addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent arg) {
				if(tfCoefPolinom.getText().compareTo("Coeficenti polinom") == 0){
					tfCoefPolinom.setForeground(Color.black);
					tfCoefPolinom.setText("");
				}
			}
			public void focusLost(FocusEvent arg) {
				if(tfCoefPolinom.getText().compareTo("") == 0){
					tfCoefPolinom.setForeground(Color.LIGHT_GRAY);
					tfCoefPolinom.setText("Coeficenti polinom");
				}
			}
		});
		
		gbc.gridx = 2;
		gbc.gridy = 0;
		this.add(btnVerifica, gbc);
		btnVerifica.setHorizontalAlignment(SwingConstants.CENTER);
		btnVerifica.setPreferredSize(new Dimension(100, 25));
		btnVerifica.setMaximumSize(new Dimension(100, 25));
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 2;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		this.add(lbPolinom, gbc);
		lbPolinom.setVisible(false);
		lbPolinom.setHorizontalAlignment(SwingConstants.CENTER);
		lbPolinom.setPreferredSize(new Dimension(610, 25));
		lbPolinom.setMaximumSize(new Dimension(610, 25));
		
	}
	
	public void addbtnVerificaActionListener(ActionListener al){
		btnVerifica.addActionListener(al);
	}
	
	public void addbtnVerificaKeyListener(KeyAdapter ka){
		btnVerifica.addKeyListener(ka);
	}
	
	public void addtfCoefPolinomKeyListener(KeyAdapter ka){
		tfCoefPolinom.addKeyListener(ka);
	}
	
	public void setbtnVerificaText(String text){
		btnVerifica.setText(text);
	}
	
	public String getbtnVerificaText(){
		return btnVerifica.getText();
	}
	
	public String gettfNumePolinom(){
		return tfNumePolinom.getText();
	}
	
	public void settfNumePolinomVisible(boolean visible){
		tfNumePolinom.setVisible(visible);
	}
	
	public void settfNumePolinom(String text){
		tfNumePolinom.setText(text);
	}
	
	public String gettfCoefPolinom(){
		return tfCoefPolinom.getText();
	}
	
	public void settfCoefPolinom(String text){
		tfCoefPolinom.setText(text);
	}
	
	public void settfCoefPolinomVisible(boolean visible){
		tfCoefPolinom.setVisible(visible);
	}
	
	public void setlbPolinomText(String text){
		lbPolinom.setText(text);
	}
	
	public void setlbPolinomVisible(boolean visible){
		lbPolinom.setVisible(visible);
	}
	
}
